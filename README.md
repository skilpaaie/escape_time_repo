## Creación del repositorio y configuración del proyecto en Unity

Se asume que se utiliza SourceTree y el editor de Unity.

1. Click en **Clone** en SourceTree, e introduce la URL de este repositorio (disponible en la pestaña de Overview, recomendable utilizar HTTPS).
2. Configurar el directorio donde crear el repositorio local y confirmar.
3. Abrir el editor de Unity y hacer click en **Open**, navegar hasta el directorio anterior y seleccionar la carpeta raíz del proyecto.
4. Cuando se abra, seleccionar la escena **Main**.
5. Fin.