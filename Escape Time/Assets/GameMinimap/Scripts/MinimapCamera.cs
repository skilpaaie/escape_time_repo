﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour {


	public bool fullMap;

	Vector3 followPlayerPosition;
	Vector3 mapPosition;

	public Transform mapPlayer;
	Transform map;

	Camera miniMapCamera;

	bool mapButton;

	GameObject Map;

	void Start() {

		Map = GameObject.FindGameObjectWithTag ("Map");
		map = Map.transform;
		fullMap = false;
		followPlayerPosition = transform.localPosition;
		mapPosition = new Vector3 (0, 500f, 0);
		miniMapCamera = gameObject.GetComponent<Camera> ();

	}

	//Lock camera rotation
	void Update () {

		transform.rotation = Quaternion.Euler(90, 0, 0);

		if (GameManager.Instance.InputController.Map)
			fullMap = !fullMap;

		StartCoroutine ("ToggleMap");

		}
			


	IEnumerator ToggleMap () {

		//Minimapa sigue al jugador
		if (!fullMap) {

			transform.SetParent (mapPlayer);
			transform.localPosition = followPlayerPosition;
			miniMapCamera.orthographicSize = 22.5f;
			transform.localPosition = new Vector3 (transform.localPosition.x, 500f, transform.localPosition.z);

		} 

		//Minimapa muestra vista global
		else {

			transform.SetParent (map);
			transform.position = mapPosition;
			miniMapCamera.orthographicSize = 78.5f;
			transform.localPosition = new Vector3 (0f, 600f, 0f);

		}

		yield return new WaitForSeconds (0.8f);
	}
}
