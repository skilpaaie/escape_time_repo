﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowUpPickUp : PickUp {

	[SerializeField] float factor;
	[SerializeField] int type; 		// 0 = change character' scale. 
	// 1 = change character' speed 
	// 2 = change bullet's damage

	// We are inside the chest's "Is trigger" collider
	void OnTriggerEnter(Collider other) {
		// Player inside the collider
		if (other.tag == "Player") {
			//We take the properties of the player inside the collider
			Player player = other.GetComponent <Player> ();
			//Pasamos el tipo de power up y el factor multiplicativo al script player
			if (player != null) {
				player.multiplierPowUp = factor;
				player.typePowUp = type;
				player.ActiveMultiplier ();
			}
		}
	}
}