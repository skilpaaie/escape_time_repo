﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectPickUp : PickUp {

	[SerializeField] int type;

	// We are inside the chest's "Is trigger" collider
	void OnTriggerEnter(Collider other) {
		// Player inside the collider
		if (other.tag == "Player") {
			//We take the properties of the player inside the collider
			Player player = other.GetComponent<Player> ();
			CollectManager collectManager = other.GetComponent<CollectManager> ();
			//We write into the power up's array 
			if (player != null) {
				player.collectableArray [type] = true;
				collectManager.CheckFinishCollect();
			}
		}
	}
}
