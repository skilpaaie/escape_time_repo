﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {

	[SerializeField]float defaultSpeed;
	[SerializeField]float aimedSpeed;

	float speed;

	Transform Reticle;
	Transform crossTop;
	Transform crossBottom;
	Transform crossLeft;
	Transform crossRight;

	bool firstAim;
	bool aiming;

	int counter;

	GameObject canvas;

	void Start() {

		if (!GetComponentInParent<Player> ().IsLocalPlayer) {

			Destroy (this.gameObject);
			return;
		}

		canvas = GameObject.FindGameObjectWithTag ("Canvas");
		Reticle = canvas.transform.Find ("Reticle");
		crossTop = Reticle.Find("Cross/Top").transform;
		crossBottom = Reticle.Find("Cross/Bottom").transform;
		crossLeft = Reticle.Find("Cross/Left").transform;
		crossRight = Reticle.Find("Cross/Right").transform;
		firstAim = false;
		aiming = false;
		counter = 0;
		speed = 0;
	}

	void Update() {

		PlayerState state = GameManager.Instance.LocalPlayer.PlayerState;

		if ((state.WeaponState == PlayerState.EWeaponState.AIMING || state.WeaponState == PlayerState.EWeaponState.AIMEDFIRING) && !aiming) {

			speed = aimedSpeed;

			if (counter != 4) {
				ApplyScale (10f);
				counter++;
			} else {
				counter = 0;
				firstAim = true;
				aiming = true;
			}

		} else if (firstAim && !(state.WeaponState == PlayerState.EWeaponState.AIMING || state.WeaponState == PlayerState.EWeaponState.AIMEDFIRING)) {

			speed = defaultSpeed;

			if (counter != 4) {
				ApplyScaleBack (10f);
				counter++;
			} else {
				counter = 0;
				firstAim = false;
				aiming = false;
			}
		}
	}

	public void ApplyScaleBack(float scale){

		crossTop.localPosition += new Vector3 (0, scale, 0);
		crossBottom.localPosition += new Vector3 (0, -scale, 0);
		crossLeft.localPosition += new Vector3 (-scale, 0, 0);
		crossRight.localPosition += new Vector3 (scale, 0, 0);
	}

	public void ApplyScale(float scale){

		crossTop.localPosition -= new Vector3 (0, scale, 0);
		crossBottom.localPosition -= new Vector3 (0, -scale, 0);
		crossLeft.localPosition -= new Vector3 (-scale, 0, 0);
		crossRight.localPosition -= new Vector3 (scale, 0, 0);
	}

}
