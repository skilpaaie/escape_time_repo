﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponReloader : MonoBehaviour {

	[SerializeField] int maxAmmo;
	[SerializeField] float reloadTime;
	[SerializeField] int clipSize;
	[SerializeField] Container inventory;
	[SerializeField] EWeaponType weaponType;

	int ammo;
	public int shotsFiredInClip;
	public bool isReloading;
	System.Guid containerItemId;

	// Evento cuando se ha cambiado de munición, para actualizar el cargador y actualizar la IU
	public event System.Action OnAmmoChanged;

	public EWeaponType getWeaponType() {

		return weaponType;
	}

	//Devuelve el número de balas que quedan en el cargador
	public int RoundsRemainingInClip {
		get {
			return clipSize - shotsFiredInClip;
		}
	}
	
	//Devuelve el número de balas que quedan en total
	public int RoundsRemainingInInventory {
		get {
			return inventory.GetAmountRemaining(containerItemId);
		}
	}
	
	//Devuelve 1 si se está recargando, 0 en caso contrario
	public bool IsReloading  {
		get {
			return isReloading;
		}
	}

	//Al inicializarse, añade los cargadores y la munición al inventario del jugador
	void Awake() {
		inventory.OnContainerReady += () => {
			containerItemId = inventory.Add(weaponType.ToString(), maxAmmo);
		};
	}

	//Función para recargar
	public void Reload() {

		if (weaponType == EWeaponType.BOW)
			return;

		if (isReloading)
			return;

		isReloading = true;

		//Ejecuta la función de recargar durante el tiempo indicado por reloadTime
		GameManager.Instance.Timer.Add (() => {
			ExecuteReload(inventory.TakeFromContainer (containerItemId, 0)); // Se quita 0 del inventario porque la munición es ilimitada
		}, reloadTime);
	}

	//Recarga el cargador y activa el evento de cambio de munición
	private void ExecuteReload(int amount) {
		isReloading = false;
		shotsFiredInClip = 0;  // Se ajusta directamente a 0 porque la munición es ilimitada

		HandleOnAmmoChanged();
	}

	//Saca el número de balas para el cargador desde el inventario del jugador
	public void TakeFromClip(int amount) {
		shotsFiredInClip += amount;
		HandleOnAmmoChanged();
	}

	// Evento
	public void HandleOnAmmoChanged() {
		if(OnAmmoChanged != null) {
			OnAmmoChanged();
		}
	}
}
