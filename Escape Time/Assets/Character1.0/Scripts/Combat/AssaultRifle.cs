﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AssaultRifle : Shooter {

//Esta es una clase derivada de la clase Shooter. Lo único que hace es llamar a la función Fire y Reload,
//que están en otro archivo
	//public float multiplier = 1;

	public override void Fire () {
		base.Fire ();

		if (canFire) {
			//We can fire the gun
		}
	}
		
	//public void Change(){
	//	powUpMultiplier = multiplier;
	//}

	public void Update () {
		
		if (player.InputState.Reload) {
			Reload ();
		}
	}

}
