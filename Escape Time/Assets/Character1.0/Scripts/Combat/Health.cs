﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Destructable {

//Es una clase derivada de Destructable que llama a las funciones de Despawn y recibir daño, que se 
//encuentran en otro archivo

	public bool dead;
	public bool damaged;

	public override void Die() {
		
		base.Die ();

		if (tag == "Player") {

			dead = true;
							
			GameManager.Instance.Respawner.Despawn (gameObject, 5f);

		}

	}

	void OnEnable () {
		Reset ();
		dead = false;
	}

	public override void TakeDamage (float amount) {
		base.TakeDamage (amount);
	}

	public bool IsDead() {

		return dead;
	}

	void Update() {

		//Para probar si la muerte funciona. Se le quita 1 de vida al personaje cada medio segundo.
		/*if (!IsDead () && !damaged) {
			TakeDamage (1f);
			damaged = true;
			GameManager.Instance.Timer.Add (() => {
				damaged=false; // Se quita 0 del inventario porque la munición es ilimitada
			}, 0.5f);	}*/
		}

	public void SetPlayerHealth(float dt) {

		damageTaken = dt;
	}	
}
