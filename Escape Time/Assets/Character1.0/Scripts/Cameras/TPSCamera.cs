﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSCamera : MonoBehaviour {

	[SerializeField]float speedRotation;

	private Vector3 screenCenter;
	public Camera camera;
	private float horizontal;
	public Transform cameraPosition;

	Transform cameraLookTarget;
	public Transform Sphere;

	private Player localPlayer;

	private Vector3 normalCamera;
	private Vector3 initialPosition;

	void Awake(){

		screenCenter.x = 0.5f;
		screenCenter.y = 0.5f;
		screenCenter.z = 0;
		horizontal = transform.eulerAngles.y;

		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;

		initialPosition = cameraPosition.localPosition;
	
		//Debug.Log (Camera.main.transform.position.magnitude - (Sphere.position).magnitude);
	}

	void HandleOnLocalPlayerJoined (Player player) {
		localPlayer = player;
		cameraLookTarget = GameManager.Instance.LocalPlayer.transform.Find ("AimingPivot");

		if (cameraLookTarget == null)
			// cameraLookTarget = localPlayer.transform;
			print("shit");
	}


	void LateUpdate() {
		
		if (GameManager.Instance.LocalPlayer != null) {

			if (GameManager.Instance.LocalPlayer.PlayerState.WeaponState == PlayerState.EWeaponState.AIMING
				|| GameManager.Instance.LocalPlayer.PlayerState.WeaponState == PlayerState.EWeaponState.AIMEDFIRING) {
				if (camera.fieldOfView != 25.0f)
					camera.fieldOfView -= 2.5f;
			} else {
				if (camera.fieldOfView != 60.0f)
					camera.fieldOfView += 2.5f;
			}

		
	
			focusRaycast ();
			//moveCamera ();

			//Posición límite de la cámara con la que queremos comparar
			Vector3 targetPosition = cameraPosition.position - GameManager.Instance.LocalPlayer.transform.forward * 0.4f;

			//Posición límite del campo de visión del jugador. El objeto Sphere se sitúa a la altura del hombro del personaje por detrás
			Vector3 collisionDestination = Sphere.transform.position + GameManager.Instance.LocalPlayer.transform.forward * 1.1f;
			HandleCameraCollision (collisionDestination, ref targetPosition);
			Debug.DrawLine (targetPosition, collisionDestination, Color.blue);

			//Si el jugador se aleja del muro, se corrige la posición de la cámara a la habitual
			UpdateDistance ();

		}
	}

	private void focusRaycast(){
		Ray cameraRay = camera.ViewportPointToRay (screenCenter);
	}

	private void moveCamera() {

		var mouseHorizontal = GameManager.Instance.InputController.MouseInput.x;
		horizontal = (horizontal + speedRotation * mouseHorizontal) % 360;
		transform.rotation = Quaternion.AngleAxis (horizontal, Vector3.up);
		
	}

	void HandleCameraCollision(Vector3 toTarget, ref Vector3 fromTarget)
	{
		// Rayo entre la cámara y el jugador cuyo objetivo es capturar los objetos entre ambos
		RaycastHit hit;
		// Si existe algún objeto entre la cámara y el personaje, la cámara se sitúa en el punto de colisión
		if (Physics.Linecast (toTarget, fromTarget, out hit)) {

			Debug.Log (hit.collider.tag);

			//Para evitar bugs desactivamos la interacción con el jugador o con los objetos
			if (hit.collider.tag == "Player" || hit.collider.tag == "Chest") {
				
				cameraPosition.position = normalCamera;
				return;
			}
			
			Vector3 hitPoint = new Vector3 (hit.point.x, hit.point.y, hit.point.z);
			fromTarget = new Vector3 (hitPoint.x, fromTarget.y, hitPoint.z);
			cameraPosition.position = fromTarget;

		} else {
			
			cameraPosition.localPosition = initialPosition;
		}
	}

	void UpdateDistance() {
		
		//Esta distancia es la mínima por defecto entre el jugador y la cámara. Si se sobrepasa esta distancia por un 10% se restaura la cámara a su valor inicial
		if (((cameraPosition.position.magnitude - (Sphere.transform.position).magnitude) > 1f) ||
			(cameraPosition.position.magnitude - (Sphere.transform.position).magnitude) < -1f) {

			cameraPosition.localPosition = initialPosition;
		}
	}
}
