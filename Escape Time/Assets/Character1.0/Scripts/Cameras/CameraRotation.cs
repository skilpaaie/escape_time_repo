﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

	[SerializeField]float speedRotation;
	private float vertical;

	void Start () {

		vertical = transform.eulerAngles.x;
	}
	
	void Update () {

		var mouseVertical = GameManager.Instance.InputController.MouseInput.y;
		vertical = (vertical - speedRotation * mouseVertical) % 360f;
		vertical = Mathf.Clamp (vertical, -27, 20);
		transform.localRotation = Quaternion.AngleAxis (vertical, Vector3.right);
	}
}
