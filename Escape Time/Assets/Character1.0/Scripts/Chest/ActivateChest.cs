﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ActivateChest : NetworkBehaviour {

	public Transform lid, lidOpen, lidClose;	// Lid, Lid open rotation, Lid close rotation
	public float openSpeed = 5F;				// Opening speed
	private bool collidingChest = false;

	//public float waitNextOpening = 3.0F;		// Time in seconds that remains open the chest after it's opened
	private float startTime;					// Take the time that the player opens the chest

	[HideInInspector]
	public bool _open;							// Is the chest opened

	[SerializeField] float rateOfInstantiate = 10.0F;	// Time in seconds to wait until you can find again powerups in the chest
	public GameObject[] powerUp;				// Objects to instantiate

	[SyncVar]									// SyncVar variables keep the same value in all clients. This value is imposed by the server.
	public int powerUpIndex;					// Index to randomize the power up to instantiate
	private float nextInstantiateAllowed;		// Time when powerups appear in the chest again
	private Transform chestCenter;				// Initial position for the powerUp when it's intantiated					

	public bool RandomItemEachTime;				// 1 = Each time, a new item is displayed

	public AudioSource audioSource;
	public AudioClip openClip;

	Player player;

	// Use this for initialization
	void Start(){
		startTime = Time.time;
		//Descomentar si queremos prefabs aleatorios
		/*if (!RandomItemEachTime){
			powerUpIndex = Random.Range (0, powerUp.Length);
		}*/
	}

	//Al inicializar, coge el centro del cofre para instanciar power ups a partir de él
	void Awake() {
		chestCenter = transform.Find("ChestCenter");
	}

	// Lines of code inside, are checked as each frame
	void Update () {

		// Player near the chest + Press button for opening chest (action button)
		if (collidingChest && player!= null && player.InputState.Action) {
			if (_open != true) { // if the chest is not open yet...
				_open = true;
				startTime = Time.time; //start the timer
				audioSource.PlayOneShot (openClip);
			}
		}

		// if rateOfInstantiate has passed since we opened the chest, we close it
		if(Time.time > startTime + rateOfInstantiate){
			_open = false;
		}

		//Control opening and closing of the chest
		if (_open) {
			ChestClicked (lidOpen.rotation);
			PowerUp ();

		} else {
			ChestClicked (lidClose.rotation);
		}
			
	}

	// Rotate the lid to the requested rotation
	void ChestClicked(Quaternion toRot){
		if(lid.rotation != toRot){
			lid.rotation = Quaternion.Lerp(lid.rotation, toRot, Time.deltaTime * openSpeed);
		}
	}
		

	// We are inside the chest's "Is trigger" collider
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			collidingChest = true;
			player = other.gameObject.GetComponent<Player> ();
		} 
	}

	// We are outside the chest's "Is trigger" collider
	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			collidingChest = false;
		}
	}


	//Instancia un powerUp o coleccionables si ha pasado el tiempo suficiente
	public void PowerUp() {

		if(Time.time < nextInstantiateAllowed)
			return;

		nextInstantiateAllowed = Time.time + rateOfInstantiate;

		// Instantiate the power up after randomizing (this way, each time you open the chest the power up is going to change)
		// if you don't want it this way you must situate the line below in Start()
		/*if (RandomItemEachTime) {
			
			powerUpIndex = Random.Range (0, powerUp.Length);
		}*/

		Instantiate(powerUp[powerUpIndex], chestCenter.position, chestCenter.rotation);
		//NetworkServer.Spawn (PowerUP);

	}

	public void SetPowerUpIndex(int index) {

		powerUpIndex = index;
	}

	[ClientRpc]
	public void RpcSpawnPowerUp(GameObject power) {

		NetworkServer.Spawn (power);
	}

}
