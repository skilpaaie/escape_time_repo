﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChestManager : NetworkBehaviour
{
	public GameObject[] chest;              // The chest prefab to be spawned.
	public int chestIndex;					// Index to randomize the chest to instantiate
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	public int numberOfChests;				// Number of chests to instantiate
	private int totalChests;				// Minimum between numberOfChests and number of spawnPoints
	private bool[] locationsUsed = new bool[50];// Se usa para señalar que en un punto ya se ha instanciado un cofre
	private bool [] indexUsed = new bool[50];
	private int spawnPointIndex;
	public bool spawnAtStart = false;
	private int counter;
	private int powerIndex;
	GameObject[] chests;


	//Los cofres hacen spawn al comenzar la partida
	public override void OnStartServer() {

		powerIndex = Random.Range (0, 2);
		Spawn ();
	}
		

	void Spawn ()

	{

		// The number of chests to instantiate will be the minimum between the lenght of spawnPoints vector and variable numberOfChests
		totalChests = Mathf.Min (spawnPoints.Length, numberOfChests);
		chests = new GameObject[totalChests];

		// Inicializamos el vector de "localizaciones usadas" a falso, ya que aún no hemos utilizado ninguna 
		locationsUsed = new bool[spawnPoints.Length];
		indexUsed = new bool[8];
		for (int j = 0; j < locationsUsed.Length; j++) {
			locationsUsed [j] = false;
		}

		for (int j = 0; j < 8; j++) {
			indexUsed [j] = false;
		}
			
		// For each chest...
		for (int i = 0; i < totalChests; i++) {
			
			// Find a random index between zero and one less than the number of spawn points.
			spawnPointIndex = Random.Range (0, spawnPoints.Length);

			//We check if this locations has been used. If so we iterate until one that it's not
			while (locationsUsed [spawnPointIndex] == true) {
				spawnPointIndex = Random.Range (0, spawnPoints.Length);
			}

			// Create an instance of the chest prefab at the randomly selected spawn point's position and rotation.
			chestIndex = Random.Range (0, chest.Length - 1);

			//Creamos una variable auxiliar en la que recogemos el cofre con su coleccionable correspondiente
			GameObject newChest = chest [chestIndex];
			SetChestSettings (newChest);

			//Instanciamos el cofre modificado
			GameObject spawnedChest = Instantiate (newChest, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

			//Registramos el nuevo prefab para que el servidor pueda instanciarlo
			ClientScene.RegisterPrefab (spawnedChest);

			//Spawn del cofre
			NetworkServer.Spawn (spawnedChest);

			//Quitamos el registro (por seguridad)
			ClientScene.UnregisterPrefab (spawnedChest);
		
			// En esta localización en la que hemos instanciado un cofre, ya no podremos instanciar más
			locationsUsed [spawnPointIndex] = true;
		}
				
	}

	public void SetChestSettings (GameObject chest) {

		//En esta función imponemos un coleccionable aleatorio, sin repetir los que ya han salido

		ActivateChest settings = chest.GetComponent<ActivateChest> ();

		if (settings == null)
			return;

		//Vamos probando diferentes coleccionables hasta que encontramos uno que no se ha usado
		while (indexUsed [powerIndex]) {

			powerIndex = Random.Range (0, settings.powerUp.Length);
				
				//Si ya se han usado los 8, metemos cualquiera al azar
				if (counter >= 7)
					break;
			counter++;
		
		}

		counter = 0;

		//Actualizamos los coleccionables ya usados
		indexUsed [powerIndex] = true;
		//Le damos al cofre el coleccionable seleccionado. Al ser SyncVar, el valor de este coleccionable se mantiene entre servidor y clientes
		chest.GetComponent<ActivateChest>().SetPowerUpIndex (powerIndex);

	}
		
}