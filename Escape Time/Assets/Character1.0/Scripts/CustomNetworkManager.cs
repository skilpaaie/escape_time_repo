﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class CustomNetworkManager : NetworkManager {

    private int chosenCharacter = 0;
    public GameObject[] characters;

    // https://forum.unity.com/threads/using-multiple-player-prefabs-with-the-network-manager.438930/

    //subclass for sending network messages
    public class NetworkMessage : MessageBase
    {
        public int chosenClass;
    }
 
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader) {
        print("OnServerAddPlayer");
        NetworkMessage message = extraMessageReader.ReadMessage<NetworkMessage>();
        int selectedClass = message.chosenClass;
        Debug.Log("Server added player with message: " + selectedClass);
 
        GameObject player;

        Transform startPos = GetStartPosition();
 
        player = Instantiate(characters[selectedClass],
            startPos == null ? Vector3.zero : startPos.position,
            startPos == null ? Quaternion.identity : startPos.rotation) as GameObject;
 
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }
 
    public override void OnClientConnect(NetworkConnection conn) {
        print("OnClientConnect");
        
        NetworkMessage test = new NetworkMessage();
        chosenCharacter = PlayerSelection.selectedPlayer;

        test.chosenClass = chosenCharacter;

        if(ClientScene.AddPlayer(conn, 0, test))
            print("Successfully added player");
    }


}
