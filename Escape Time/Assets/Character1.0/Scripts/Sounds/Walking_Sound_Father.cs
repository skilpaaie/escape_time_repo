﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking_Sound_Father : MonoBehaviour {

	[SerializeField]
	private AudioClip[] clips;

	private AudioSource audioSource;

	private void Awake (){
		audioSource = GetComponent<AudioSource> ();
	}

	private void Walking_Sound(){

		AudioClip clip = GetRandomClip();
		audioSource.PlayOneShot(clip);
	}

	private AudioClip GetRandomClip(){
		return clips[UnityEngine.Random.Range(0,clips.Length)];
	}
}