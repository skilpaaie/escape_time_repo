﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Landing_Roll_Father : MonoBehaviour {

	[SerializeField]
	private AudioClip[] clips;

	private AudioSource audioSource;

	private void Awake (){
		audioSource = GetComponent<AudioSource> ();
	}

	private void Landing_Roll(){

		AudioClip clip = GetRandomClip();
		audioSource.PlayOneShot(clip);
	}

	private AudioClip GetRandomClip(){
		return clips[UnityEngine.Random.Range(0,clips.Length)];
	}
}