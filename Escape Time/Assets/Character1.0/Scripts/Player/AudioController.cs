﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour {

	[SerializeField]AudioClip[] clips;
	[SerializeField]float delayBetweenClips;

	private bool canPlay;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		canPlay = true;
	}

	public void Play() {

		if (!canPlay)
			return;


		//Añade sonido a los pasos cada cierto tiempo, dado por la variable delayBetweenClips
		GameManager.Instance.Timer.Add (() => {canPlay = true;}, delayBetweenClips);

		canPlay = false;

		//Función que activa el sonido
		AudioClip clip = clips[Random.Range (0, clips.Length)];
		source.PlayOneShot (clip);
	}
}
