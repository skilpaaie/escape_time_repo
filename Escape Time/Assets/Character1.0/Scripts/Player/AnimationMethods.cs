﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMethods : MonoBehaviour {

	public CharacterController cc;
	public PlayerAnimation playerAnimator;

	public float impulse;
	public float roll_impulse;
	private float gravity = 0.5f;

	public float verticalVelocity;
	float horizontalVelocity;
	float frontalVelocity;

	bool left;
	bool right;
	bool forward;
	bool backward;

	public PlayerShoot playerShoot;
	private AudioSource audioSource;
	public AudioClip reloadClip;
	public AudioClip looseClip;
	public AudioClip loadingClip;
	public AudioClip stepClip;
	public AudioClip jumpStepClip;
	public AudioClip runStepClip;

	void Start() {

		audioSource = gameObject.GetComponent<AudioSource> ();
	}
	//Se llama cuando se pulsa el botón de salto. Impulsa el personaje hacia arriba
	public void JumpForce() {

		verticalVelocity = 0;

		verticalVelocity += impulse;

	}

	//Se llama cuando se pulsa el botón de rodar hacia la izquierda. Impulsa el personaje hacia la izquierda
	public void JumpForceLeft() {

		horizontalVelocity = 0;
		horizontalVelocity -= impulse;
		left = true;


	}

	//Se llama cuando se pulsa el botón de rodar hacia la derecha. Impulsa el personaje hacia la derecha
	public void JumpForceRight() {
		
		horizontalVelocity = 0;
		horizontalVelocity += impulse;
		right = true;
	}

	//Se llama cuando se pulsa el botón de rodar hacia atrás. Impulsa el personaje hacia atrás
	public void JumpForceBack() {
		
		frontalVelocity = 0;
		frontalVelocity -= impulse;
		backward = true;
	}

	//Se pulsa cuando se pulsa el botón de rodar hacia adelante. Impulsa el personaje hacia adelante
	public void RollJump() {

		frontalVelocity = 0;
		frontalVelocity += impulse;
		forward = true;

	}

	public void ExecuteChangeUp() {

		int direction = 1;

		playerShoot.currentWeaponIndex += direction;
		if (playerShoot.currentWeaponIndex > playerShoot.weapons.Length -1) {
			playerShoot.currentWeaponIndex = 0;
		}

		if (playerShoot.currentWeaponIndex < 0) {
			playerShoot.currentWeaponIndex = playerShoot.weapons.Length -1;
		}

		playerShoot.Equip(playerShoot.currentWeaponIndex);

	}

	public void ExecuteChangeDown() {

		int direction = -1;

		playerShoot.currentWeaponIndex += direction;
		if (playerShoot.currentWeaponIndex > playerShoot.weapons.Length -1) {
			playerShoot.currentWeaponIndex = 0;
		}

		if (playerShoot.currentWeaponIndex < 0) {
			playerShoot.currentWeaponIndex = playerShoot.weapons.Length -1;
		}

		playerShoot.Equip(playerShoot.currentWeaponIndex);

	}

	void Update() {

		checkVelocities ();

		verticalVelocity -= gravity * Time.deltaTime;

		cc.Move (horizontalVelocity * GameManager.Instance.LocalPlayer.transform.right + 
			verticalVelocity * GameManager.Instance.LocalPlayer.transform.up +
			frontalVelocity * GameManager.Instance.LocalPlayer.transform.forward);

		if (verticalVelocity <= 0 && cc.isGrounded)
			verticalVelocity = 0;



	}

	void checkVelocities() {

		if (left) {
			
			horizontalVelocity += gravity * Time.deltaTime;

			if (horizontalVelocity >= 0) {

				horizontalVelocity = 0;
				left = false;
			}

		}

		if (right) {

			horizontalVelocity -= gravity * Time.deltaTime;

			if (horizontalVelocity <= 0) {

				horizontalVelocity = 0;
				right = false;
			}

		}

		if (backward) {

			frontalVelocity += gravity * Time.deltaTime;

			if (frontalVelocity >= 0) {

				frontalVelocity = 0;
				backward = false;
			}

		}

		if (forward) {

			frontalVelocity -= gravity * Time.deltaTime;

			if (frontalVelocity <= 0) {

				frontalVelocity = 0;
				forward = false;
			}

		}
	}
		
	public void Disparar(){

		playerShoot.ActiveWeapon.Fire ();
	}



	//------------------------------SONIDOS----------------------------//

	public void ReloadSound() {

		audioSource.PlayOneShot(reloadClip);
	}

	private void Loosing(){

		audioSource.PlayOneShot(looseClip);
	}

	private void Loading(){

		audioSource.PlayOneShot(loadingClip);
	}

	private void CrawlingStep(){

		audioSource.PlayOneShot(stepClip);
	}

	private void JumpingStep(){

		audioSource.PlayOneShot(jumpStepClip);
	}

	private void RunningStep(){

		audioSource.PlayOneShot(runStepClip);
	}
		
}
