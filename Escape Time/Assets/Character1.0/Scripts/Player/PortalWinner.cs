﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalWinner : MonoBehaviour {

	// We are inside the chest's "Is trigger" collider
	void OnTriggerEnter(Collider other) {
		// Player inside the collider
		if (other.tag == "Player") {
			//We take the properties of the player inside the collider
			Player player = other.GetComponent <Player> ();
			if (player != null) {
				if (player.allCollectsPicked == true) {
					player.winner = true;
					print ("You have won");
				}
			}
		}
	}
}
