﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Es la máquina de estados de las animaciones

public class PlayerAnimation : MonoBehaviour {

	private Animator animator;

	public float Vertical;
	public float Horizontal;
	public bool isWalking;
	public bool isSprinting;
	public bool isCrouched;
	public float aimAngle;
	public bool isAiming;
	public bool isJumping;
	public bool isRolling;
	public bool isShooting;
	public bool isReloading;
	public bool isDead;


	private PlayerAim mPlayerAim;
	private PlayerAim PlayerAim {
		get {
			if(mPlayerAim == null)
				mPlayerAim = Player.playerAim;
			return mPlayerAim;
		}
	}

	private Player mPlayer;
	public Player Player {

		get { 

			if (mPlayer == null)
				mPlayer = GetComponent<Player> ();
			return mPlayer;
		}
	}
		
	void GetLocalPlayerInput() {

		Vertical = Player.InputState.Vertical;
		Horizontal = Player.InputState.Horizontal;

		isWalking = GameManager.Instance.LocalPlayer.PlayerState.MoveState == PlayerState.EMoveState.WALKING;
		isSprinting = Player.InputState.IsSprinting;
		isCrouched = Player.InputState.IsCrouched;

		aimAngle = PlayerAim.GetAngle ();
		isAiming = GameManager.Instance.LocalPlayer.PlayerState.WeaponState == PlayerState.EWeaponState.AIMING
		|| GameManager.Instance.LocalPlayer.PlayerState.WeaponState == PlayerState.EWeaponState.AIMEDFIRING;
		isJumping = Player.InputState.Jump;
		isRolling = Player.InputState.Roll;
		isShooting = Player.InputState.Fire1;
		isDead = Player.PlayerHealth.IsDead ();
		isReloading = Player.PlayerShoot.ActiveWeapon.Reloader.IsReloading;	

	}

	void Awake () {
		animator = GetComponentInChildren<Animator> ();
	}

	//Activa las variables tipo bool necesarias para la máquina de estados de las animaciones

	void Update () {


		if (Player.IsLocalPlayer)
			GetLocalPlayerInput ();
		
		animator.SetFloat ("Vertical", Vertical);
		animator.SetFloat ("Horizontal", Horizontal);

		animator.SetBool ("IsWalking", isWalking);
		animator.SetBool ("IsSprinting", isSprinting);
		animator.SetBool ("IsJumping", isJumping);

		animator.SetBool ("IsGrounded", GameManager.Instance.InputController.CheckGrounded ());
		animator.SetBool ("IsFalling", GameManager.Instance.InputController.CheckFalling ());
		animator.SetBool ("IsRolling", isRolling);


		if (gameObject.name.Contains("Arquera")) {

			animator.SetBool ("IsShooting", isShooting);

		} 
		else {

			animator.SetBool ("IsCrouched", isCrouched);
			animator.SetBool ("Excited", GameManager.Instance.InputController.Excited);
			animator.SetFloat("AimAngle", aimAngle);
			animator.SetBool("IsAiming",isAiming);
			animator.SetBool ("IsSwitchingUp", GameManager.Instance.InputController.WeaponUp);
			animator.SetBool ("IsSwitchingDown", GameManager.Instance.InputController.WeaponDown);
			animator.SetBool ("IsDead", isDead);
			animator.SetBool ("IsReloading", isReloading);

		}

	}

}
