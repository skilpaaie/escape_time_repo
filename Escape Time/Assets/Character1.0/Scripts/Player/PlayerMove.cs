﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	InputController.InputState playerInput;

	private Player mPlayer;

	public Player Player {

		get { 

			if (mPlayer == null)
				mPlayer = GetComponent<Player> ();
			return mPlayer;
		}
	}

	private CharacterController mMoveController;

	public CharacterController MoveController {

		get { 

			if (mMoveController == null)
				mMoveController = GetComponent<CharacterController> ();
			return mMoveController;
		}
	}

	void Awake() {

		playerInput = GameManager.Instance.InputController.State;
	}

	public void SetInputController (InputController.InputState state) {

		playerInput = state;
	}


	void Move() {

		if (playerInput == null) {

			playerInput = GameManager.Instance.InputController.State;
			if (playerInput == null)
				return;
		}

		Move (playerInput.Horizontal, playerInput.Vertical);
	}

	public void Move (float horizontal, float vertical) {

		float moveSpeed = Player.Settings.runSpeed;

		if (playerInput.IsWalking)
			moveSpeed = Player.Settings.walkSpeed;
		
		if (playerInput.IsSprinting)
			moveSpeed = Player.Settings.sprintSpeed;
		
		if (playerInput.IsCrouched)
			moveSpeed = Player.Settings.crouchSpeed;

		Vector2 direction = new Vector2 (vertical * moveSpeed * Player.multiplierSpeed, horizontal * moveSpeed * Player.multiplierSpeed);
		MoveController.SimpleMove (transform.forward * direction.x + transform.right * direction.y);

	}

	void Update() {

		if (!Player.IsLocalPlayer)
			return;

		if ((GameManager.Instance.LocalPlayer.PlayerState.MoveState == PlayerState.EMoveState.DEAD))
			return;

		if (!GameManager.Instance.IsNetworkGame) {
			Move ();
		}
	}
}
