using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Máquina de estados del personaje

public class PlayerState : MonoBehaviour {

	public Health playerHealth;

	public enum EMoveState {
		WALKING,
		RUNNING,
		CROUCHING,
		SPRINTING,
		DEAD
	}

	public enum EAirState {

		GROUNDED,
		AIR

	}

	public enum EWeaponState {
		IDLE,
		FIRING,
		AIMING,
		AIMEDFIRING
	}

	public EMoveState MoveState;
	public EAirState AirState;
	public EWeaponState WeaponState;

	private InputController mInputController;
	public InputController InputController {
		get {
			if (mInputController == null) {
				mInputController = GameManager.Instance.InputController;
			}
			return mInputController;
		}
	}

	void Update() {
		SetWeaponState();
		SetMoveState();
		SetAirState ();
	}

	void SetWeaponState() {
		WeaponState = EWeaponState.IDLE;

		if(InputController.Fire1)
			WeaponState = EWeaponState.FIRING;
		
		if(InputController.Fire2)
			WeaponState = EWeaponState.AIMING;

		if(InputController.Fire1 && InputController.Fire2)
			WeaponState = EWeaponState.AIMEDFIRING;
	}

	void SetMoveState() {
		MoveState = EMoveState.RUNNING;

		if (InputController.IsSprinting)
			MoveState = EMoveState.SPRINTING;
		
		if (InputController.IsWalking)
			MoveState = EMoveState.WALKING;

		if (InputController.IsCrouched)
			MoveState = EMoveState.CROUCHING;
		
		if (playerHealth.IsDead())
			MoveState = EMoveState.DEAD;
	}

	void SetAirState() {

		if (InputController.IsGrounded)
			
			AirState = EAirState.GROUNDED;
		
		if (!InputController.IsGrounded)
			
			AirState = EAirState.AIR;
		
	}
	
}
