﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;



//En este script se gestionan todas las acciones del jugador

[RequireComponent(typeof(PlayerState))]
public class Player : MonoBehaviour  {

	//------------------------------------------------------------------------

	//Mouse control
	[System.Serializable]
	public class MouseInput {
		public Vector2 Damping;
		public Vector2 Sensitivity;
		public bool lockMouse;
	}

	//Move
	public Character Settings;
	public MouseInput MouseControl;

	//Power Up
	private bool powerUpActive = false;
	public float multiplierPowUp;
	public int typePowUp;
	[SerializeField] float timeMultiplierPowUp;
	private float finishMultiplierPowUp;
	public bool[] powUpActiveArray = new bool[3];

	private float multiplierSize = 1;
	public float multiplierSpeed = 1;
	private float multiplierDamage = 1;

	//Collectable
	public bool[] collectableArray = new bool[100];

	//Winning the game
	public bool allCollectsPicked = false;
	public bool winner = false;
	GameObject textObject;
	Text [] UITexts;
	Text centralText;
	private ExitNetwork exitManager;

	//Audio controller
	[SerializeField]AudioController footsteps;

	//Aim controller
	public PlayerAim playerAim;

	public bool IsLocalPlayer;

	public Camera mainCamera;
	public Camera minimapCamera;

	//UI
	GameObject canvas;
	//------------------------------------------------------------------------

	//Instancia el controlador que mueve al personaje bidireccionalmente, según el fichero MoveController.cs

	private InputController.InputState mInputState;
	public InputController.InputState InputState {
		get {
			if (mInputState == null) 
				mInputState = GameManager.Instance.InputController.State;
			return mInputState;
		}
	}


	//Instancia el controlador que permite al personaje disparar, según el fichero PlayerShoot.cs
	private PlayerShoot mPlayerShoot;
	public PlayerShoot PlayerShoot {
		get {
			if (mPlayerShoot == null) 
				mPlayerShoot = GetComponent<PlayerShoot>();
			return mPlayerShoot;
		}
	}

	//Instancia la máquina de estados del personaje.
	private PlayerState mPlayerState;
	public PlayerState PlayerState{
		get {
			if (mPlayerState == null){
				mPlayerState = GetComponent<PlayerState>();
			}
			return mPlayerState;
		}
	}

	//Puntos de salud del personaje
	private Health mPlayerHealth;
	public Health PlayerHealth{
		get {
			if (mPlayerHealth == null){
				mPlayerHealth = GetComponent<Health>();
			}
			return mPlayerHealth;
		}
	}

	private InputController playerInput;
	private Vector2 mouseInput;

	//En la inicialización instancia el control del teclado y oculta el cursor del ratón
	void Awake () {

		mainCamera.enabled = false;
		minimapCamera.enabled = false;
		
		if(!GameManager.Instance.IsNetworkGame)
			SetAsLocalPlayer ();		
	}

	public void SetInputState(InputController.InputState state) {
		mInputState = state;
	}

	public void SetAsLocalPlayer() {

		IsLocalPlayer = true;
		canvas = GameObject.FindGameObjectWithTag ("Canvas");
		UITexts = canvas.gameObject.GetComponentsInChildren<Text> ();
		exitManager = GameObject.FindGameObjectWithTag ("ExitManager").GetComponent<ExitNetwork>();

		for (int i = 0; i < UITexts.Length; i++) {

			if (UITexts [i].gameObject.name == "CentralText") {

				centralText = UITexts [i];
				break;
			}
		}

		centralText.text = "";

		playerInput = GameManager.Instance.InputController;
		GameManager.Instance.LocalPlayer = this;

		if (MouseControl.lockMouse) {
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}

		mainCamera.enabled = true;
		minimapCamera.enabled = true;
	}

	// Actualiza la posición del personaje y de la cámara, así como la dirección en la que se apunte (LookAround)
	void Update () {

		if (GameFinished.IsGameFinished && IsLocalPlayer) {

			if (!winner)
				centralText.text = "You lost! Press Start to return";

			if (Input.GetButton ("Start")) {
				GameFinished.IsGameFinished = false;

				if (exitManager != null)
					GameFinished.Exit = true;

			}
			
			
			return;
		}

		if (PlayerHealth.dead && IsLocalPlayer) {

			centralText.color = Color.red;
			centralText.text = "You have been killed!";
			return;

		} else {

			if (IsLocalPlayer) {
				centralText.color = Color.black;
				if (!GameFinished.IsGameFinished)
					centralText.text = " ";
			}
		}

		// Cuando termina el efecto del powerUp, sus efectos se desactivan
		if(Time.time > finishMultiplierPowUp && powerUpActive == true){
			powerUpActive = false;
			PowerUpMultiplier ();
		}

		// We prompted in the HUD a message if the player has won
		if (winner == true) {
			
			GameFinished.IsGameFinished = true;

			if(IsLocalPlayer)
				centralText.text = "You have won! Press Start to return";
			
		}

		if (IsLocalPlayer) {
			// FIX ME!
			LookAround ();

		}

		Scale ();


	}

	void Scale() {
		transform.localScale = new Vector3(1, 1, 1) * multiplierSize;
	}
		
	//Actualiza la posición de la dirección a la que apunta el personaje según el cursor del ratón
	void LookAround () {
		
		mouseInput.x = Mathf.Lerp(mouseInput.x, playerInput.MouseInput.x, 1f / MouseControl.Damping.x);
		mouseInput.y = Mathf.Lerp(mouseInput.y, playerInput.MouseInput.y, 1f / MouseControl.Damping.y);

		transform.Rotate(Vector3.up * mouseInput.x * MouseControl.Sensitivity.x);
		playerAim.SetRotation(mouseInput.y * MouseControl.Sensitivity.y);
	}

	// POWER - UPs
	// Se llama a esta función desde el script "PowerUpPickUp" cuando el player entra en el collider de un power up
	public void ActiveMultiplier() {
		// player dentro del collider
		powerUpActive = true;

		//En función del tipo de collider activamos una habilidad. 
		//Con un bool marcamos qué power ups están activos. Este bool se usa en IUPowerUp que actúa sobre la HUD
		if (typePowUp == 0){			// 0 = change character' scale. 
			multiplierSize = multiplierPowUp;
			powUpActiveArray [0] = true;
		}
		else if (typePowUp == 1){ 		// 1 = change character' speed 
			multiplierSpeed = multiplierPowUp;
			powUpActiveArray [1] = true;
		}

		else if (typePowUp == 2){		// 2 = change bullet's damage
			multiplierDamage = multiplierPowUp;
			PlayerShoot.ActiveWeapon.powUpMultiplier = multiplierDamage;
			powUpActiveArray [2] = true;
			//assaultRifle.Change ();

		}

		//Llamamos a la función que se muestra en las siguientes líneas
		PowerUpMultiplier ();
	}

	// Si se ha cogido un power up, se calcula el tiempo en que dejará de hacer efecto. 
	// Se llama desde la función ActiveMultiplier y desde el Update una vez pasado el tiempo de power up
	public void PowerUpMultiplier() {

		if (powerUpActive) {
			finishMultiplierPowUp = Time.time + timeMultiplierPowUp;
		} else {
			multiplierPowUp  = 1;
			RestoreMultiplier ();
		}

	}

	//Pasado un tiempo fijado por timeMultiplierPowUp, el power up deja de hacer efecto y se restauran los valores
	public void RestoreMultiplier(){
		multiplierSize	 = multiplierPowUp;
		multiplierSpeed	 = multiplierPowUp;
		multiplierDamage = multiplierPowUp;
		PlayerShoot.ActiveWeapon.powUpMultiplier = multiplierDamage;

		for (int j = 0; j < powUpActiveArray.Length; j++) {
			powUpActiveArray [j] = false;
		}
	}

}