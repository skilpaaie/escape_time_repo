﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

	[SerializeField]float weaponSwitchTime;

	public Shooter[] weapons;
	Shooter activeWeapon;

	public int currentWeaponIndex;
	bool canFire;
	Transform weaponHolster;
	Transform aimPivot;

	Player player;

	//Evento de cambio de arma
	public event System.Action<Shooter> OnWeaponSwitch;

	//Devuelve el arma que esté activa
	public Shooter ActiveWeapon {
		
		get {
			return activeWeapon;
		}
	}

	void Start() {
		player = GetComponent<Player>();
	}

	//Al inicializar, pone el arma activa (el primer componente del vector de armas) en las manos del personaje
	//y activa la variable que permite al jugador disparar
	void Awake() {
		canFire = true;
		weaponHolster = transform.Find("Weapons");
		weapons = weaponHolster.GetComponentsInChildren<Shooter>();
		aimPivot = transform.Find ("AimingPivot");

		if(weapons.Length > 0) {
			Equip(0);
		}
	}

	//Desactiva el arma que estuviese activa y la quita de las manos del personaje
	void DeactivateWeapons() {
		for (int i = 0; i < weapons.Length; i++){
			weapons[i].gameObject.SetActive(false);
			weapons[i].transform.SetParent(weaponHolster);
		}
	}

	//Cambia de arma. Básicamente avanza por el vector de armas, en función de la variable direction
	void SwitchWeapon(int direction) {

		if (weapons.Length <= 1)
			return;

		//Impide el disparo mientras se cambie de arma
		canFire = false;

		//Añade un tiempo de cambio de arma, dado por la variable weaponSwitchTime
		GameManager.Instance.Timer.Add(() => {
		}, weaponSwitchTime);
	}
		
	//Equipa el arma que se encuentra en la posición "index" del vector de armas
	public void Equip(int index) {
		DeactivateWeapons();
		canFire = true;
		activeWeapon = weapons[index];
		activeWeapon.Equip();
		weapons[index].gameObject.SetActive(true);

		if(OnWeaponSwitch != null)
			OnWeaponSwitch(activeWeapon);
	}

	public Vector3 GetImpactPoint() {
		
		Ray ray = Camera.main.ViewportPointToRay(new Vector3 (1f, 1f, 0));
	
		RaycastHit hit;
		Vector3 targetPosition = ray.GetPoint(1000);	

		if (Physics.Raycast(ray, out hit))
			return hit.point;
	
		return targetPosition;
	}


	//Cambia de arma o dispara en función de los inputs del teclado
	void Update()  {

		if(!player.IsLocalPlayer /* && IsPlayerAlive*/ ) {
			if(player.InputState.Fire1) {

				if (activeWeapon.name != "Bow") {

					print ("The other player is firing!");
					//		player.CmdFire ();
					activeWeapon.Fire ();

				}
			}
		}

		if(player.IsLocalPlayer) {
			if(GameManager.Instance.InputController.WeaponUp)
				SwitchWeapon(1);

			if (GameManager.Instance.InputController.WeaponDown) 
				SwitchWeapon(-1);

				//Si está esprintando, no permite el disparo y se sale directamente de la función
			if(player.PlayerState.MoveState == PlayerState.EMoveState.SPRINTING)
				return;

			if (!canFire)
				return;

			if(player.InputState.Fire1){

					activeWeapon.SetAimPoint (GetImpactPoint ());
				
				if (activeWeapon.name!="Bow") {

					//player.CmdFire ();
					activeWeapon.Fire ();
					print ("I am firing!");

				}
			} 
		}
	}
}
