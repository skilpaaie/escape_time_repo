﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;

[RequireComponent(typeof(Player))]
public class PlayerNetwork : NetworkBehaviour {


	Player player;
	PlayerMove playerMove;
	PlayerAnimation playerAnimation;
	public Transform aimPivot;
	float mouseInputY;

	NetworkState state;	
	NetworkState lastSentState;
	NetworkState lastSentRpcState;
	NetworkState lastReceivedState;

	//Asignar coleccionables
	bool asigned;

	List<NetworkState> predictedStates;

	[System.Serializable]
	public partial class NetworkState : InputController.InputState
	{
		public float PositionX;
		public float PositionY;
		public float PositionZ;
		public float AimTargetX;
		public float AimTargetY;
		public float AimTargetZ;
		public float RotationAngleY;
		public float TimeStamp;
		public int DamageTaken;
		public bool IsDead;
		//Asignación coleccionables
		public int [] CollectsToPick;
		public bool [] CollectablesPicked;
	}

	void Start () {

		player = GetComponent<Player> ();
		state = new NetworkState ();
		playerMove = GetComponent<PlayerMove> ();
		playerAnimation = GetComponent<PlayerAnimation> ();

		predictedStates = new List<NetworkState>();

		if (isLocalPlayer) {
			
			player.SetAsLocalPlayer ();
		}

		asigned = false;
	}


	// Toma del jugador local todos los inputs necesarios para ser enviados al servidor
	private NetworkState CollectInput() {

		var state = new NetworkState {
			
			Fire1 = GameManager.Instance.InputController.Fire1,
			Fire2 = GameManager.Instance.InputController.Fire2,
			Horizontal = GameManager.Instance.InputController.Horizontal,
			Vertical = GameManager.Instance.InputController.Vertical,
			IsCrouched = GameManager.Instance.InputController.IsCrouched,
			IsSprinting = GameManager.Instance.InputController.IsSprinting,
			IsWalking = GameManager.Instance.InputController.IsWalking,
			Reload = player.PlayerShoot.ActiveWeapon.Reloader.IsReloading,
			IsGrounded = GameManager.Instance.InputController.IsGrounded,
			IsFalling = GameManager.Instance.InputController.IsFalling,
			Jump = GameManager.Instance.InputController.Jump,
			Roll = GameManager.Instance.InputController.Roll,
			Action = GameManager.Instance.InputController.Action,
			RotationAngleY = transform.rotation.eulerAngles.y,
			TimeStamp = Time.time,
			DamageTaken = (int) GetComponent<Player>().PlayerHealth.damageTaken,
			IsDead = GetComponent<Health>().dead,
			AimAngle = GetComponent<Player>().MouseControl.Sensitivity.y * mouseInputY,
			IsAiming = GameManager.Instance.InputController.Fire2,
			CollectsToPick = player.GetComponent<CollectManager> ().collectsToPick,
			CollectablesPicked = player.collectableArray

		};

		if(state.Fire1) {
			
			Vector3 shootingSolution = player.PlayerShoot.GetImpactPoint();
			state.AimTargetX = shootingSolution.x;
			state.AimTargetY = shootingSolution.y;
			state.AimTargetZ = shootingSolution.z;
		}

		return state;
	}

	void Update() {
		
		mouseInputY = Mathf.Lerp(mouseInputY, GameManager.Instance.InputController.MouseInput.y, 1f/player.MouseControl.Damping.y);

		if (isLocalPlayer) {

			state = CollectInput ();
			playerMove.SetInputController (state);
			playerMove.Move (state.Horizontal, state.Vertical);
		}
			
		if (lastReceivedState == null)
			return;

		UpdateState ();
	}

	void UpdateState() {
			
		Vector3 serverPosition = new Vector3(lastReceivedState.PositionX, lastReceivedState.PositionY, lastReceivedState.PositionZ);

		if (isLocalPlayer && !isServer) {

			//remove the old states (if there are any)
			predictedStates.RemoveAll (x => x.TimeStamp < lastReceivedState.TimeStamp);

			//we expect the state to be here
			var predictedState = predictedStates.Where (x => x.TimeStamp == lastReceivedState.TimeStamp).First ();

			Vector3 predictedPosition = new Vector3 (predictedState.PositionX, predictedState.PositionY, predictedState.PositionZ);
			float positionDifferenceFromServer = Vector3.Distance (predictedPosition, serverPosition);

			if (positionDifferenceFromServer > .9f)
				transform.position = Vector3.Lerp (transform.position, serverPosition, player.Settings.runSpeed * Time.deltaTime);

		}
	
		if (!isLocalPlayer) {

			playerAnimation.Vertical = lastReceivedState.Vertical;
			playerAnimation.Horizontal = lastReceivedState.Horizontal;
			playerAnimation.isWalking = lastReceivedState.IsWalking;
			playerAnimation.isSprinting = lastReceivedState.IsSprinting;
			playerAnimation.isCrouched = lastReceivedState.IsCrouched;
			playerAnimation.isAiming = lastReceivedState.IsAiming;
			playerAnimation.aimAngle = player.playerAim.GetAngle();
			playerAnimation.isJumping = lastReceivedState.Jump;
			playerAnimation.isRolling = lastReceivedState.Roll;
			playerAnimation.isReloading = lastReceivedState.Reload;
			playerAnimation.isDead = lastReceivedState.IsDead;

			if(player.PlayerShoot.ActiveWeapon.name == "Bow")
				playerAnimation.isShooting = lastReceivedState.Fire1;

			Vector3 shootingSolution =  new Vector3(lastReceivedState.AimTargetX, lastReceivedState.AimTargetY, lastReceivedState.AimTargetZ);

			player.playerAim.SetRotation (lastReceivedState.AimAngle);

			playerMove.SetInputController(lastReceivedState);
			player.SetInputState(lastReceivedState);
			player.PlayerHealth.UpdateDamage (lastReceivedState.DamageTaken);

			if(shootingSolution != Vector3.zero)
				player.PlayerShoot.ActiveWeapon.SetAimPoint(shootingSolution);

			transform.eulerAngles = new Vector3(
				transform.transform.rotation.eulerAngles.x,
				lastReceivedState.RotationAngleY,
				transform.transform.rotation.eulerAngles.z);

			playerMove.Move(lastReceivedState.Horizontal, lastReceivedState.Vertical);

			if (!isServer) {
				
				float positionDifferenceFromServer = Vector3.Distance(transform.position, serverPosition);
				if (positionDifferenceFromServer > .9f)
					transform.position = Vector3.Lerp(transform.position, serverPosition, player.Settings.runSpeed * Time.deltaTime);
			}

			//Asignamos los coleccionables al jugador no local una vez solamente
			if (!asigned) {

					if (lastReceivedState.CollectsToPick != null) {

						player.GetComponent<CollectManager> ().collectsToPick = lastReceivedState.CollectsToPick;
						asigned = true;
					}

			}

			//Actualización coleccionables
			UpdateCollectables();
		}

	}

	void FixedUpdate() {

		if (isLocalPlayer) {

			if (isInputStateDirty (state, lastSentState)) {

				lastSentState = state;
				Cmd_HandleInput (SerializeState (lastSentState));

				state.PositionX = transform.position.x;
				state.PositionY = transform.position.y;
				state.PositionZ = transform.position.z;
				state.AimAngle = mouseInputY * GetComponent<Player> ().MouseControl.Sensitivity.y;

				predictedStates.Add (state);

			}
		}

		// If we are the server, update the remote clients.
		if(isServer && lastReceivedState != null) {
			
			NetworkState stateSolution = new NetworkState {
				PositionX = transform.position.x,
				PositionY = transform.position.y,
				PositionZ = transform.position.z,
				Horizontal = lastReceivedState.Horizontal,
				Vertical = lastReceivedState.Vertical,
				Fire1 = lastReceivedState.Fire1,
				Fire2 = lastReceivedState.Fire2,
				IsAiming = lastReceivedState.IsAiming,
				IsCrouched = lastReceivedState.IsCrouched,
				IsFalling = lastReceivedState.IsFalling,
				IsGrounded = lastReceivedState.IsGrounded,
				IsSprinting = lastReceivedState.IsSprinting,
				IsWalking = lastReceivedState.IsWalking,
				Reload = lastReceivedState.Reload,
				Jump = lastReceivedState.Jump,
				Roll = lastReceivedState.Roll,
				Action = lastReceivedState.Action,
				AimAngle = lastReceivedState.AimAngle,
				RotationAngleY = lastReceivedState.RotationAngleY,
				TimeStamp = lastReceivedState.TimeStamp,
				DamageTaken = lastReceivedState.DamageTaken,
				IsDead = lastReceivedState.IsDead,
				CollectsToPick = lastReceivedState.CollectsToPick,
				CollectablesPicked = lastReceivedState.CollectablesPicked
			};

			if (stateSolution.Reload)
				stateSolution.Fire1 = false;

			if (isInputStateDirty(stateSolution, lastSentRpcState)) {
				lastSentRpcState = stateSolution;
				Rpc_HandleStateSolution(SerializeState(lastSentRpcState));
			}
		}
	}

	[Command]
	void Cmd_HandleInput(byte[] data) {

		lastReceivedState = DeserializeState (data);
	}

	[ClientRpc]
	void Rpc_HandleStateSolution(byte[] data) {
		lastReceivedState = DeserializeState(data);

	}

	bool isInputStateDirty(NetworkState a, NetworkState b) {

		if (b == null)
			return true;

		return a.AimAngle != b.AimAngle ||
		a.Fire1 != b.Fire1 ||
		a.Fire2 != b.Fire2 ||
		a.Horizontal != b.Horizontal ||
		a.Vertical != b.Vertical ||
		a.AimAngle != b.AimAngle ||
		a.IsAiming != b.IsAiming ||
		a.IsCrouched != b.IsCrouched ||
		a.IsSprinting != b.IsSprinting ||
		a.IsWalking != b.IsWalking ||
		a.RotationAngleY != b.RotationAngleY ||
		a.Reload != b.Reload ||
		a.IsGrounded != b.IsGrounded ||
		a.IsFalling != b.IsFalling ||
		a.Jump != b.Jump ||
		a.Roll != b.Roll ||
		a.Action != b.Action ||
		a.DamageTaken != b.DamageTaken ||
		a.IsDead != b.IsDead ||
		a.CollectsToPick != b.CollectsToPick ||
		a.CollectablesPicked != b.CollectablesPicked;

	}

	private BinaryFormatter bf = new BinaryFormatter();

	private byte[] SerializeState(NetworkState state) {

		using (MemoryStream stream = new MemoryStream ()) {

			bf.Serialize (stream, state);
			return stream.ToArray();
		}
	}

	private NetworkState DeserializeState(byte[] bytes) {

		using (MemoryStream stream = new MemoryStream (bytes))
			return (NetworkState)bf.Deserialize (stream);
	}

	public void UpdateCollectables() {

		if (lastReceivedState.CollectablesPicked == null || player.collectableArray == null || !asigned)
			return;

		for (int i = 0; i < player.collectableArray.Length; i++) {

			if (player.collectableArray [i] != lastReceivedState.CollectablesPicked [i])
				
				player.collectableArray [i] = lastReceivedState.CollectablesPicked [i];
		}
	}
}
