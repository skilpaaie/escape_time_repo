﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	[System.Serializable]
	public class InputState
	{
		public float Vertical;
		public float Horizontal;
		public bool Fire1;
		public bool Fire2;
		public bool Reload;
		public bool IsWalking;
		public bool IsSprinting;
		public bool IsCrouched;
		public bool IsGrounded;
		public bool IsFalling;
		public bool Jump;
		public bool Roll;
		public bool Action;
		public float AimAngle;
		public bool IsAiming;

	}

	public float Vertical {get {return State.Vertical;}}
	public float Horizontal {get {return State.Horizontal;}}
	public bool Fire1 {get {return State.Fire1;}}
	public bool Fire2 {get {return State.Fire2;}}
	public bool Reload {get {return State.Reload;}}
	public bool IsWalking {get {return State.IsWalking;}}
	public bool IsSprinting {get {return State.IsSprinting;}}
	public bool IsCrouched {get {return State.IsCrouched;}}
	public bool IsGrounded {get {return State.IsGrounded;}}
	public bool IsFalling {get {return State.IsFalling;}}
	public bool Jump {get {return State.Jump;}}
	public bool Roll {get {return State.Roll;}}
	public bool Action {get {return State.Action;}}
	public float AimAngle {get {return State.AimAngle;}}
	//public bool IsAiming {get {return State.IsAiming;}}

	public Vector2 MouseInput;

	public bool WeaponUp;
	public bool WeaponDown;
	public bool Excited;
	public bool Map;

	public InputState State;


	void Start() {
		
		State = new InputState ();
	}

	void Update () {

		//Controles del teclado
		State.Vertical = Input.GetAxis ("Vertical");
		State.Horizontal = Input.GetAxis ("Horizontal");
		Vector2 joystickInput = new Vector2 (Input.GetAxis("RightJoystickHorizontal"),Input.GetAxis("RightJoystickVertical"));
		MouseInput = new Vector2 (
			Input.GetAxisRaw ("Mouse X") + joystickInput.x,
			Input.GetAxisRaw ("Mouse Y") + joystickInput.y);

		State.Fire1 = Input.GetButton("Fire1");
		State.Fire2 = Input.GetButton("Fire2");
		State.Reload = Input.GetButton ("Reload");
		
		State.IsWalking = Input.GetButton("Walk") || (Input.GetAxis("Walk_Trigger") != 0);
		State.IsSprinting = Input.GetButton("Sprint");
		State.IsCrouched = Input.GetButton("Crouch");

		WeaponUp = Input.GetKey (KeyCode.Q) || Input.GetAxis("Mouse ScrollWheel") > 0;
		WeaponDown = Input.GetAxis("Mouse ScrollWheel") < 0;
		
		State.Jump = Input.GetButton("Jump");
		State.Roll = Input.GetAxis("Roll") != 0;
		State.Action = Input.GetButton("Action");
		Excited = Input.GetKey (KeyCode.F1);
		Map = Input.GetButtonDown("Map");

		if (GameManager.Instance.LocalPlayer != null) {
			
			State.IsGrounded = CheckGrounded ();
			State.IsFalling = CheckFalling ();

		}
	}


	public bool CheckGrounded() {

		RaycastHit hitInfo;

		bool Check = Physics.Raycast (GameManager.Instance.LocalPlayer.transform.position, Vector3.down, out hitInfo, 0.4f);

		//if (!Check)
		//print ("Grounded");

		return Check;

	}

	public bool CheckFalling() {

		RaycastHit hitInfo;

		bool Check = Physics.Raycast (GameManager.Instance.LocalPlayer.transform.position, Vector3.down, out hitInfo, 0.8f + 0.5f);

		//if (Check)
		//print ("Falling");

		return Check;

	}
}
