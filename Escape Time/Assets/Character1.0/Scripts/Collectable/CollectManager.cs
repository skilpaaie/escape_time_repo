﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CollectManager : NetworkBehaviour {

	public int[] collectsToPick;        		// Array that keep traceability of collects picked.
	public int numberOfCollectsToPick = 4;		// Number of distinct collectables each player has to look for
	public int totalCollects = 8;				// Total number of collectables
	private bool[] collectsYetAssigned;			// Se usa para señalar que en un punto ya se ha asignado este collectable
	private int index;							// Se usa para iterar
	private Player player;						// Necesitamos este script para conocer los coleccionables que debe buscar el Player
	private ChestManager chestManager;			// Necesitamos este script para acceder a los spawn points de portal y así poder instanciarlo
	private int counter = 0;

	void Start () {
		// Call AssignCollects function to assign collects to pick to each player
		AssignCollects ();
		chestManager = GameObject.FindGameObjectWithTag ("PortalManager").GetComponent<ChestManager> ();
	
		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;

	}

	void HandleOnLocalPlayerJoined (Player pl) {

		player = pl;

	}


	void AssignCollects ()
	{

		// Inicializamos el vector de "collects ya asignados" a falso, ya que aún no hemos asignado ninguna 
		collectsToPick = new int[numberOfCollectsToPick];
		collectsYetAssigned = new bool[totalCollects];

		// For each collect...
		for (int i = 0; i < numberOfCollectsToPick; i++) {

			// Find a random index between zero and one less than the number of total collects.
			index = Random.Range (0, totalCollects);

			//We check if this collects has already been assigned. If so we iterate until one that it's not
			while (collectsYetAssigned [index] == true) {
				index = Random.Range (0, totalCollects);
			}

			//Asignamos el collect a la lista que tiene que buscar el player
			collectsToPick [i] = index;

			// Este collect ya lo hemos asignado, ya no podremos asignarlo más
			collectsYetAssigned [index] = true;
		}			
	}

	public void CheckFinishCollect(){

		if (GetComponent<Player>().collectableArray.Length == 0 || collectsToPick.Length == 0)
			return;

		counter = 0;
		for (int i = 0; i < numberOfCollectsToPick; i++) {
			
			if (GetComponent<Player>().collectableArray [collectsToPick [i]]) {
				counter += 1;
			}
		}
		if (counter == numberOfCollectsToPick) {
			// The player has collected all the collectables required
			GetComponent<Player>().allCollectsPicked = true;
			// Create an instance of the portal prefab at the randomly selected spawn point's position and rotation.
			int spawnIndex = Random.Range (0, chestManager.spawnPoints.Length);
			chestManager.chestIndex = Random.Range (0, chestManager.chest.Length);
			GameObject portal = Instantiate (chestManager.chest[chestManager.chestIndex], 
				chestManager.spawnPoints[chestManager.chestIndex].position, new Quaternion (0,0,0,1));

			ClientScene.RegisterPrefab (portal);
			NetworkServer.Spawn (portal);
			ClientScene.UnregisterPrefab (portal);
		}
	}
}
