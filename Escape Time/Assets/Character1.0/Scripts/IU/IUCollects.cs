﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IUCollects : MonoBehaviour {

	public Image customImage;					// imagen que se muestra en el espacio del HUD definido por collectType
	[SerializeField]  Sprite[] imageArray;		// array de imágenes formado por las imágenes en gris (números pares) y a color (numeros impares)
	private Player player;						// referencia al script Player
	private CollectManager collectManager;		// referencia al script CollectManager
	[SerializeField] int collectType;			// sirve para determinar en cuál de los 4 huecos que hay en la HUD situamos la imagen

	// Buscamos al jugador para conocer qué collects tiene que buscar
	void Start () {
		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;
	}

	void HandleOnLocalPlayerJoined (Player pl) {

		player = pl;
		collectManager = player.GetComponent<CollectManager> ();
	}

	// Si el player ha cogido el collect, lo mostramos a color. En caso contrario, en blanco y negro
	void Update () {

		if (player == null)
			return;

		// Si el player ha cogido el collect...
		if (player.collectableArray [collectManager.collectsToPick [collectType]] == true) {
			//... imagen a color
			customImage.sprite = imageArray [collectManager.collectsToPick [collectType] * 2 + 1];
		} else
			//... imagen en blanco y negro
			customImage.sprite = imageArray [collectManager.collectsToPick [collectType] * 2];
	}
}
