using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AmmoCounter : MonoBehaviour {

	[SerializeField] Text text;

	PlayerShoot playerShoot;
	WeaponReloader reloader;

	Player localPlayer;

	void Awake() {
		
		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;
	}

	void HandleOnLocalPlayerJoined(Player player) {
		localPlayer = player;
		playerShoot = localPlayer.PlayerShoot;
		playerShoot.OnWeaponSwitch += HandleOnWeaponSwitch;

		if(GameManager.Instance.IsNetworkGame)
			HandleOnWeaponSwitch(playerShoot.ActiveWeapon);
	}

	void HandleOnWeaponSwitch(Shooter activeWeapon) {
		reloader = activeWeapon.Reloader;
		reloader.OnAmmoChanged += HandleOnAmmoChanged;
		HandleOnAmmoChanged();
	}

	void HandleOnAmmoChanged() {
		int amountInInventory = reloader.RoundsRemainingInInventory;
		int amountInClip = reloader.RoundsRemainingInClip;

		if (reloader.getWeaponType() == EWeaponType.BOW) {
			text.text = string.Format("∞/∞", amountInClip);
		} else {
			text.text = string.Format("{0}/∞", amountInClip);
		}
	}
		
}
