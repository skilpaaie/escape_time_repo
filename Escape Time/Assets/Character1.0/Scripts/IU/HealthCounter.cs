﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthCounter : MonoBehaviour {

	[SerializeField] Text text;
	[SerializeField] Rect healthBar;
	public RectTransform bar;

	Health playerHealth;
	Player localPlayer;

	private float defaultWidth;

	void Awake() {
		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;
		defaultWidth = healthBar.width;
	}

	void HandleOnLocalPlayerJoined(Player player) {
		
		localPlayer = player;
		playerHealth = localPlayer.PlayerHealth;
		playerHealth.OnDamageReceived += HandleOnDamageReceived;
		HandleOnDamageReceived ();

	}

	void HandleOnDamageReceived() {

		int totalHealthPoints = (int) playerHealth.hitPoints;
		int hitPointsRemaining = (int) playerHealth.HitPointsRemaining;

		int percentage = (100 * hitPointsRemaining) / totalHealthPoints;

		text.text = percentage.ToString();
		healthBar.width = defaultWidth * percentage / 100;
		bar.sizeDelta = new Vector2 (healthBar.width, healthBar.height);

	}

	void Update(){

		if(localPlayer!=null)
			HandleOnDamageReceived ();

	}



}
