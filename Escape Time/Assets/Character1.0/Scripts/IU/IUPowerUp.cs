﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IUPowerUp : MonoBehaviour {

	public Image customImage;
	private Player player;
	[SerializeField] int powUpType;

	// Buscamos al jugador para conocer qué power ups tiene activas
	void Start () {
		GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;
	}

	void HandleOnLocalPlayerJoined (Player pl) {

		player = pl;
	}
	
	// En función de qué power up tenga activas el player, habilitamos una imagen u otra
	void Update () {

		if (player == null)
			return;

		if (player.powUpActiveArray[powUpType] == true) {
			customImage.enabled = true;
		}
		else 
			customImage.enabled = false;
	}
}
