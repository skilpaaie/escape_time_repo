﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class ExitNetwork : NetworkBehaviour {


	void Update() {

		if (GameFinished.Exit == true) {

			GameManager.Instance.Timer.Add (() => {

				GameFinished.Exit = false;

				NetworkManager.singleton.StopClient ();
				NetworkManager.singleton.StopHost ();

				NetworkLobbyManager.singleton.StopClient ();
				NetworkLobbyManager.singleton.StopServer ();

				NetworkServer.DisconnectAll ();
			
				SceneManager.UnloadSceneAsync (SceneManager.GetActiveScene ().buildIndex);
				SceneManager.LoadScene("Menu");
			
			}, 5f);

		}

	}


}
