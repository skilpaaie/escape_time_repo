﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Esta clase es la genérica para las armas. Gestiona el disparo, el recargar, equiparse...

public class Shooter : MonoBehaviour {

	[SerializeField] float rateOfFire;
	[SerializeField] Projectile projectile;
	[SerializeField] Transform hand;
	//El servidor spawnea GameObjects, por lo que necesitamos el prefab completo de la bala
	[SerializeField] GameObject bulletPrefab;

	public GameObject playerObject;
	public Player player;

	public AudioClip fireClip;
	public AudioSource audioSource;

	public Vector3 aimPoint;
	public Vector3 AimTargetOffset;
	public Transform aimPivot;

	public float powUpMultiplier = 1;

	private ParticleSystem muzzleFireParticleSystem;

	private WeaponRecoil m_WeaponRecoil;
	private WeaponRecoil WeaponRecoil {
		get {
			if (m_WeaponRecoil == null)
				m_WeaponRecoil = GetComponent<WeaponRecoil>();
			return m_WeaponRecoil;
		}
	}

	//Recargador del arma
	private WeaponReloader reloader;
	public WeaponReloader Reloader{
		get {
			return reloader;
		}
	}

	private float nextFireAllowed;
	private Transform muzzle;

	public bool canFire;

	public void SetAimPoint(Vector3 target) {
		aimPoint = target;
	}

	//Pone el arma en la mano del personaje
	public void Equip() {
		transform.SetParent(hand);
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
	}

	//Al inicializar, coge la mirilla y el cargador del arma para realizar operaciones con ellos
	void Awake() {
		muzzle = transform.Find("Model/Muzzle");
		player = GetComponentInParent<Player>();		
		reloader = GetComponent<WeaponReloader>();
		muzzleFireParticleSystem = muzzle.GetComponent<ParticleSystem>();
	}

	//Llama a la función Reload, que se encuentra en otro archivo
	public void Reload() {
		if (reloader == null)
			return;

		if (player.IsLocalPlayer)
			reloader.Reload();
	}

	// Inicia el sistema de partículas en la boca del arma
	void FireEffect() {
		if(muzzleFireParticleSystem == null) 
			return;
		muzzleFireParticleSystem.Play();
	}

	//Dispara balas según una cierta frecuencia, dada por el tipo de arma

	public virtual void Fire() {
		canFire = false;
		
		if(Time.time < nextFireAllowed)
			return;

		if(player.IsLocalPlayer && reloader != null) {

			if(reloader.IsReloading)
				return;
			if(reloader.RoundsRemainingInClip == 0) {
				reloader.Reload();
				return;
			}

			reloader.TakeFromClip(1);
		}

		nextFireAllowed = Time.time + rateOfFire;

		muzzle.LookAt(aimPoint + AimTargetOffset);

		muzzle.rotation = aimPivot.rotation;

		projectile.multiplier = powUpMultiplier;
		projectile.transform.localScale = new Vector3 (1, 1, 1) * powUpMultiplier * 3;

		Projectile newBullet = Instantiate (projectile, muzzle.position, muzzle.rotation);
		audioSource.PlayOneShot (fireClip);

		if (this.WeaponRecoil)
			this.WeaponRecoil.Activate ();

		FireEffect ();

		canFire = true;
	}


}
