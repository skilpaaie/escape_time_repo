﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	[SerializeField] float prefabSpeed;
	[SerializeField] float prefabTimeToLive;

	//Cuando el power up se cree (al abrir un cofre), que desaparezca pasado cierto tiempo
	void Start() {
		Destroy(gameObject, prefabTimeToLive);
	}

	//Actualiza la posición del power up para que siga una trayectoria hacia el cielo
	void Update() {
		transform.Translate(new Vector3 (0, 1, 0) * prefabSpeed * Time.deltaTime);
		transform.Rotate (new Vector3(0, 45, 0) * 2 * Time.deltaTime);
	}
		
}