﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Esta clase dota de salud y capacidad de morir o destruirse a los objetos del juego

[RequireComponent(typeof(Collider))]
public class Destructable : MonoBehaviour {

	//Vida máxima
	public float hitPoints;

	//Eventos de muerte y daño recibido
	public event System.Action OnDeath;
	public event System.Action OnDamageReceived;

	public float damageTaken;

	//Devuelve los puntos de salud restantes
	public float HitPointsRemaining {
		get {
			return hitPoints - damageTaken;
		}
	}

	//Devuelve 1 si el objeto está vivo, 0 en caso contrario
	public bool IsAlive  {
		get {
			return HitPointsRemaining > 0;
		}
	}

	//Llama a la función onDeath cuando el objeto se queda sin puntos de salud
	public virtual void Die() {
		
		if(!IsAlive) {
			return;
		}

		if (OnDeath != null){
			OnDeath();
		}
	}

	//Resta la cantidad amount a la vida del objeto
	public virtual void TakeDamage(float amount) {

		if (HitPointsRemaining == 0)
			return;

		damageTaken += amount;

		//Si el objeto no se ha quedado sin vida, se llama al evento de recibir daño
		if(OnDamageReceived != null)
			OnDamageReceived();

		//Si el objeto se ha quedado sin vida, se llama a la función de morir
		if (HitPointsRemaining <= 0) {
			damageTaken = hitPoints;
			Die ();
		}
	}

	//Resta la cantidad amount a la vida del objeto
	public virtual void UpdateDamage(float amount) {

		if (HitPointsRemaining == 0)
			return;

		damageTaken = amount;

		//Si el objeto no se ha quedado sin vida, se llama al evento de recibir daño
		if(OnDamageReceived != null)
			OnDamageReceived();

		//Si el objeto se ha quedado sin vida, se llama a la función de morir
		if (HitPointsRemaining <= 0) {
			damageTaken = hitPoints;
			Die ();
		}
	}

	public void Reset() {
		damageTaken = 0;
	}

	void Update() {

		if (HitPointsRemaining <= 0) {
			damageTaken = hitPoints;
			Die ();
		}
	}
}
