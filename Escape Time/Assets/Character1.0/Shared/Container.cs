﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//Esta es una clase que almacena los diferentes objetos del jugador y los administra en función de los inputs del teclado

public class Container : MonoBehaviour {

	//Esta clase es un objeto que puede pertencer al container. Se le asigna un ID, un número de objetos de este tipo
	//y permite añadir o quitar objetos del container

	[System.Serializable]
	public class ContainerItem {
		public System.Guid Id;
		public string Name;
		public int Maximum;

		private int amountTaken;

		public ContainerItem() {
			Id = System.Guid.NewGuid();
		}

		public int Remaining {
			get {
				return Maximum - amountTaken;
			}
		}

		public int Get(int value) {
			if((amountTaken + value) > Maximum) {
				int tooMuch = (amountTaken + value) - Maximum;
				amountTaken = Maximum;
				return value - tooMuch;
			}
			
			amountTaken += value;
			return value;
		}

		public void Set(int amount) {
			amountTaken -= amount;
			if(amountTaken < 0) 
				amountTaken = 0;
		}
	}

	public List<ContainerItem> items;
	public event System.Action OnContainerReady;

	// Cuando se inizializa se crea una lista de objetos para el contenedor (munición, armas, coleccionables...)
	void Awake() {
		items = new List<ContainerItem>();
		if(OnContainerReady != null){
			OnContainerReady();
		}
	}

	//Añade un ID y un nombre al objeto
	public System.Guid Add(string name, int maximum) {
		items.Add(new ContainerItem {
			Maximum = maximum,
			Name = name
		});

		return items.Last().Id; 
	}

	//Añade un objeto u objetos al contenedor
	public void Put(string name, int amount) {
		var containerItem = items.Where(x => x.Name == name).FirstOrDefault();
		if(containerItem == null)
			return;
		containerItem.Set(amount);
	}

	//Saca un objeto dado por su ID del contenedor
	public int TakeFromContainer(System.Guid id, int amount) {
		var containerItem = GetContainerItem(id);	
		if(containerItem == null)
			return -1;
		return containerItem.Get(amount);
	}

	//Devuelve el número de objetos del ID dado que queden en el contenedor
	public int GetAmountRemaining(System.Guid id) {
		var containerItem = GetContainerItem(id);
		if(containerItem == null)
			return -1;
		return containerItem.Remaining;
	}

	private ContainerItem GetContainerItem(System.Guid id) {
		var containerItem = items.Where(x => x.Id == id).FirstOrDefault();
		return containerItem;
	}
}
