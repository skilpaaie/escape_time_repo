﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Función que desactiva el personaje muerto y lo vuelve a activar pasados unos segundos

public class Respawner : MonoBehaviour {

	Transform spawnPosition;

	public void Despawn(GameObject go, float inSeconds) {

		GameManager.Instance.Timer.Add (() => {
			go.transform.position = spawnPosition.position;
			go.transform.rotation = spawnPosition.rotation;

			go.GetComponent<Health>().Reset();
			go.GetComponent<Health>().dead = false;
		}, inSeconds);

	}

	void Awake() {

		spawnPosition = transform;
	}
		

}
