﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameFinished {

	private static bool mGameFinished;
	public static bool IsGameFinished {

		get { 

			return mGameFinished;
		}

		set { 

			mGameFinished = value;
		}
	}

	private static bool mExit;
	public static bool Exit {

		get { 

			return mExit;
		}

		set {

			mExit = value;

		}
	}


}
