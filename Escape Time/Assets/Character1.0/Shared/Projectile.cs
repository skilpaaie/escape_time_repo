﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Función que gestiona las balas.

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour {

	public float speed;
	[SerializeField] float timeToLive;
	[SerializeField] float damage;
	public float multiplier = 1;
	[SerializeField] Transform bulletHole;

	// Vector donde estará el punto donde existe colisión con el proyectil
	Vector3 destination = Vector3.zero;

	Vector3 prevPosition;

	//Cuando el proyectil se cree (al disparar), que desaparezca pasado cierto tiempo
	void Start() {

		Destroy(gameObject, timeToLive);

		prevPosition = transform.position;
	}

	//Actualiza la posición de la bala para que siga la trayectoria a la que se apunte (raycast)
	void Update() {

		prevPosition = transform.position;
		
		if(isDestinationReached()) {

			Destroy(gameObject);
			return;
		}

		transform.Translate(Vector3.forward * speed * Time.deltaTime);

		if(destination != Vector3.zero)
			return;

		RaycastHit hit;
		if(Physics.Raycast(prevPosition, (transform.position - prevPosition).normalized, out hit, (transform.position - prevPosition).magnitude)) {
			CheckDestructable(hit);
		}
	}

	//Si el objeto al que ha impactado la bala es del tipo Destructable, quitarle vida
	void CheckDestructable(RaycastHit hitInfo) {

		var destructable = hitInfo.transform.GetComponent<Destructable>();
		
		// El punto donde existe la colisión del proyectil
		destination = hitInfo.point + hitInfo.normal * 0.0015f;

		Vector3 newPosition = new Vector3 (destination.x, destination.y, destination.z) + hitInfo.transform.forward * 1f;

		Transform hole;

		if (gameObject.CompareTag ("Arrow"))
			hole = (Transform)Instantiate (bulletHole, newPosition, Quaternion.LookRotation (hitInfo.normal) * Quaternion.Euler (270f, 0, 0));
		else {
			
			if (hitInfo.transform.GetComponent<Player> () == null)
				hole = (Transform)Instantiate (bulletHole, destination, Quaternion.LookRotation (hitInfo.normal) * Quaternion.Euler (0, 180f, 0));
			else
				hole = null;
		}

		if(hole != null)
			hole.SetParent(hitInfo.transform);

		if (destructable == null)
			return;

		destructable.TakeDamage(damage * multiplier);
	}

	bool isDestinationReached() {
		if (destination == Vector3.zero)
			return false;

		Vector3 directionToDestination = destination - transform.position;

		// Producto escalar
		float dot = Vector3.Dot(directionToDestination, transform.forward);

		if (dot < 0) 
			return true; // Hemos pasado el punto destination
		
		return false;
	}
}
