using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Para recoger objetos. Creo que ahora mismo no está implementada

public class PickupItem : MonoBehaviour {

	void OnTriggerEnter(Collider collider) {
		if (collider.tag != "Player") {
			return;
		} 
			
		Pickup(GetComponent<Collider>().transform);
	}

	public virtual void OnPickup(Transform item) {
		
	}

	void Pickup(Transform item) {
		OnPickup(item);
	}
}
