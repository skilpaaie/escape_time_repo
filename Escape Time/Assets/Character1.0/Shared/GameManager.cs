﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//El GameManager administra todas las acciones que ocurren en la partida

public class GameManager {

	public event System.Action<Player> OnLocalPlayerJoined;
	private GameObject gameObject;

	private bool mIsNetworkedGame;
	private bool mIsNetworkedGameChecked;

	//Rutina de instanciar: Crea los componentes necesarios para controlar la partida
	private static GameManager mInstance;
	public static GameManager Instance {
		get {
			if(mInstance == null){
				mInstance = new GameManager();
				mInstance.gameObject = new GameObject("_gameManager");
				mInstance.gameObject.AddComponent<InputController>();
				mInstance.gameObject.AddComponent<Timer>();
				mInstance.gameObject.AddComponent<Respawner>();
			}
			return mInstance;
		}
	}

	public bool IsNetworkGame {

		get { 

			if (!mIsNetworkedGameChecked) {

				mIsNetworkedGameChecked = true;
				mIsNetworkedGame = GameObject.Find ("NetworkManager") != null;
			}

			return mIsNetworkedGame;
			
		}

	}

	//Controlador del teclado
	private InputController mInputController;
	public InputController InputController {
		get {
			if(mInputController == null) {
				mInputController = gameObject.GetComponent<InputController>();
			}
			return mInputController;
		}
	}

	//Temporizador. Útil para realizar otras rutinas en el resto del código
	private Timer mTimer;
	public Timer Timer {
		get {
			if(mTimer == null)
				mTimer = gameObject.GetComponent<Timer>();
			return mTimer;
			
		}
	}

	//Respawner. Para que los jugadores vuelvan a la vida cuando los maten, pasado cierto tiempo.
	private Respawner mRespawner;
	public Respawner Respawner {
		get {
			if(mRespawner == null)
				mRespawner = gameObject.GetComponent<Respawner>();
				return mRespawner;
		}
	}

	//Jugador.
	private Player mLocalPlayer;
	public Player LocalPlayer {
		get {
			return mLocalPlayer;
		}
		set {
			mLocalPlayer = value;
			if(OnLocalPlayerJoined != null)
				OnLocalPlayerJoined(mLocalPlayer);
		}
	}
		


}
