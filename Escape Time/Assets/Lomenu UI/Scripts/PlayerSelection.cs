﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerSelection  {

	private static int playerId;
	public static int selectedPlayer{
		get { 
			return playerId;
		}
		set { 
			playerId = value;
		}
	}
}
